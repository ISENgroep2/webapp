(function() {

	
	var canvas = document.getElementById('canvas'),
	context = canvas.getContext('2d'),
	video = document.getElementById('video')
	audio = document.getElementById('audio')
	vendorURL = window.URL || window.webkitURL;

	navigator.getUserMedia =  navigator.getUserMedia ||
                           navigator.webkitGetUserMedia ||
                           navigator.mozGetUserMedia ||
                           navigator.msGetUserMedia;
	navigator.getUserMedia({
		video: true,
		audio: true
	}, function(stream){
		video.src = vendorURL.createObjectURL(stream);
		video.play();
		audio.src = vendorURL.createObjectURL(stream);
		audio.play();
	}, function(error) { 
            // An error occured
            // error.code
            console.log("error");
        });

	document.getElementById("snap").addEventListener("click", function() 
	{
				context.drawImage(video, 0, 0, 400, 300);
			});
	
})();

